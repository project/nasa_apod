# NASA Astronomy Picture of the Day

Provides a page and block with NASA's Astronomy Image of the Day.

## Installation

Install the module using your preferred method following the [Installing Modules documentation](https://www.drupal.org/docs/extending-drupal/installing-modules).

## Suggested Configuration

NASA does not require an API key to use the API associated with this module.
However, without an API key, rate limiting calls will be introduced. To apply
for an key, visit [https://api.nasa.gov/index.html#apply-for-an-api-key](https://api.nasa.gov/index.html#apply-for-an-api-key).

Once you have received your API key, log in to your website and browse to 
Administration &raquo; Configuration &raquo; Web Services &raquo; Astronomy Picture of the Day Settings

## Usage

Once enabled, you will be able to use the following features:

* There will be a block named &quot;_Astronomy Picture of the Day Block_&quot; 
under Structure &raquo; Block Layout. You can add this block as you would any 
other Drupal Block.
* The full sized image can be accessed by going to _{site_url}/astronomy-picture-of-the-day_ or 
_{site_url}/astronomy-picture-of-the-day/{date}_ where {date} is any valid date either in the 
_YYYY-MM-DD_ format or a UNIX timestamp.