<?php

namespace Drupal\nasa_apod\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\nasa_apod\Service\NASAAPODService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block that displays NASA's Astronomy Picture of the Day (APOD).
 *
 * @Block(
 *   id = "nasa_apod_block",
 *   admin_label = @Translation("NASA APOD Block"),
 *   category = @Translation("NASA API")
 * )
 */
class NasaApodBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\nasa_apod\Service\NASAAPODService $api
   */
  protected $api;

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\apod\Service\APODService $apodService
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, NASAAPODService $service) {
    $this->api = $service;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'use_random_image' => '',
      'image_date' => '',
    ];
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('nasa_apod.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['image_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Image date'),
      '#description' => $this->t('The date for the image to display. If no date is entered, the current date will be used.'),
      '#default_value' => $this->configuration['image_date'],
      '#attributes' => [
        'placeholder' => $this->t('Optional'),
      ],
    ];

    $form['use_random_image'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Random image'),
      '#description' => $this->t('If checked, a random image will be displayed.'),
      '#default_value' => $this->configuration['use_random_image'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    // If the count is set, we can only use that field in the api call.
    if (!empty($values['use_random_image'])) {
      // start and end dates can't be used with the count so empty them.
      $form_state->setValue('image_date', '');
    }
    else {
      $max_date = DrupalDateTime::createFromTimestamp(NASAAPODService::FIRST_IMAGE_DATE);
      // @todo Need to check dates to see if they are past the first date of the service.
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['use_random_image'] = trim($values['use_random_image']);
    $this->configuration['image_date'] = trim($values['image_date']);
    \Drupal\Core\Cache\Cache::invalidateTags(['nasa_apod_block']);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $count = NULL;
    $image_date = new DrupalDateTime('now');

    if (!empty($this->configuration['use_random_image'])) {
      $count = 100;
      $image_date = NULL;
    }
    else {
      if (!empty($this->configuration['image_date'])) {
        $image_date = DrupalDateTime::createFromTimestamp(strtotime($this->configuration['image_date']));
      }
    }

    $data = $this->api->getImage($image_date, TRUE, $count);
    // Something went wrong with the API call so return an empty array.
    if ( $data === FALSE ) {
      return [];
    }
    $image = is_array($data) ? $data[rand(0, count($data) - 1)] : $data;
    $link = Link::createFromRoute(t('Learn more'), 'nasa_apod.date_page', ['date' => $image->date]);

    return [
      '#theme' => 'nasa_apod_block',
      '#data' => [
        'image' => (array)$image,
        'link' => $link,
      ],
      '#cache' => [
        'max-age' => 3600,
        'tags' => ['nasa_apod_block']
      ],
      '#attached' => [
        'library' => [
          'nasa_apod/default_block'
        ]
      ]
    ];
  }

  /**
   * Retrieve NASA APOD from API.
   */
  private function getApod(string $date = '') {
    $data = [
      'apod_title' => '',
      'date' => '',
      'img' => '',
      'explanation' => '',
      'copyright' => '',
      'error' => FALSE,
    ];
    $config = \Drupal::config('nasa_apod.settings');
    $query_params = [
      'date' => empty($date) ? date("Y-m-d") : $date,
      'hd' => empty($config->get('hi_res')) ? FALSE : TRUE,
      'api_key' => $config->get('key'),
    ];
    $url = $config->get('url') . '?' . http_build_query($query_params);

    try {
      $response = \Drupal::httpClient()->get($url);
      $response = (string) $response->getBody();
      $response = json_decode($response);
      $data['title'] = $response->title;
      $data['date'] = date("F j, Y", strtotime($response->date));
      $data['img'] = $config->get('hi_res') ? $response->hdurl : $response->url;
      $data['explanation'] = $response->explanation;
      $data['copyright'] = $response->copyright ? $response->copyright : '';
    }
    catch (RequestException $e) {
      $response = (string) $e->getResponse()->getBody();
      $response = json_decode($response);
      if ($response->error->code === 'API_KEY_MISSING') {
        $error = 'You need to set your API key in the NASA APOD module configuration.';
      } else {
        $error = $response->error->message;
      }
      $data['title'] = 'Oops... An error occurred!';
      $data['explanation'] = $error;
      $data['error'] = TRUE;
    }
    return $data;
  }

}
