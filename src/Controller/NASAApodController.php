<?php

/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 6/2/16
 * Time: 3:36 PM
 */
namespace Drupal\nasa_apod\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\nasa_apod\Service\NASAAPODService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NASAApodController extends ControllerBase {

  /**
   * @var \Drupal\nasa_apod\Service\NASAAPODService
   */
  private $api;

  const ONE_DAY = 86400;
  const APOD_DATE_DEFAULT_FORMAT = 'Y-m-d';

  public function __construct(NASAAPODService $service) {
    $this->api = $service;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nasa_apod.api')
    );
  }

  public function index() {
    $today = new DrupalDateTime('now', date_default_timezone_get() );
    return $this->redirect('nasa_apod.date_page', ['date' => $today->format('Y-m-d')]);
  }

  public function content($date=NULL) {
    // First Astronomy Picture of the day appears to be July 1, 1995.
    $first_image = DrupalDateTime::createFromTimestamp( NASAAPODService::FIRST_IMAGE_DATE );
    $today = new DrupalDateTime('now');
    $today->setTime(0,0,0);

    if ( !empty($date) ) {
      if ( is_string( $date ) && preg_match( '/[0-9]{4}(\-[0-9]{2}){2}/', $date ) ) {
        $date = DrupalDateTime::createFromTimestamp( strtotime( $date ) );
      } else {
        $date = $today;
      }
    } else {
      $date = $today;
    }

    if ( $date->getTimestamp() < $first_image->getTimestamp() ) {
      $this->messenger()->addWarning('NASA\'s API only contains image from June 16, 1995 forward.');
      return $this->redirect('nasa_apod.date_page',['date' => $today->format('Y-m-j')]);
    }

    /*
     * The NASA api doesn't let you get images for dates in the future.
     */
    if ( $date->format('U') > $today->format('U')) {
      throw new NotFoundHttpException();
    }

    $image = $this->api->getImage($date, TRUE);

    if ( $image !== FALSE ) {
      /*
     * Set up an empty array for our pagination.
     */
      $items = array();

      if ( $date->format('U') > $first_image->format('U') ) {
        $previous_date = DrupalDateTime::createFromTimestamp( $date->format('U') - self::ONE_DAY );
        $items[] = Link::fromTextAndUrl($this->t( '&laquo; Previous' ), Url::fromRoute( 'nasa_apod.date_page', array( 'date' => $previous_date->format( self::APOD_DATE_DEFAULT_FORMAT ) ) ) );
      }

      if ( $date->format('U') < $today->format('U') ) {
        $next_date = DrupalDateTime::createFromTimestamp( $date->format('U') + self::ONE_DAY );
        $items[] = Link::fromTextAndUrl($this->t( 'Next &raquo;' ), Url::fromRoute( 'nasa_apod.date_page', array( 'date' => $next_date->format( self::APOD_DATE_DEFAULT_FORMAT ) ) ) );
      }

      $build['content'] = array(
        '#theme' => 'nasa_apod_content',
        '#title' => array('#plain_text' => $image->title),
        '#image' =>array(
          '#theme' => ($image->media_type == 'video' ? "nasa_apod_video" : "nasa_apod_image"),
          '#item' => (array)$image,
          '#attached' => array(
            'library' =>  array(
              'nasa_apod/default_page'
            ),
          ),
        ),
        '#image_date' => $date,
        '#links' => array(
          '#theme' => 'item_list',
          '#items' => $items,
          '#list_type' => 'ul',
          '#attributes' => array(
            'id' => 'nasa-apod-navigation',
            'class' => array('pager__items')
          ),
        ),
        '#description' => check_markup($image->explanation),
        '#copyright' => check_markup(($image->copyright ?? '')),
        '#cache' => [
          'max-age' => 600,
          'contexts' => ['url'],
          'tags' => ['nasa_apod_page']
        ]
      );
    }
    else {
      $build = [
        '#markup' => '<h3 class="nasa-apod-error">' . $this->t('Opps! We are unable to display the image from NASA. Please try again later.') . '</h3>',
        '#attached' => array(
          'library' =>  array(
            'nasa_apod/default_page'
          ),
        ),
      ];
    }

    return $build;

  }
}