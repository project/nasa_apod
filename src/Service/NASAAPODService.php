<?php
/**
 * Created by PhpStorm.
 * User: Ronald Ferguson
 * Date: 5/23/16
 * Time: 9:34 AM
 */

namespace Drupal\nasa_apod\Service;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;


class NASAAPODService {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig $config
   */
  private $config;

  /**
   * @var \Drupal\Core\Messenger\Messenger
   */
  private $messenger;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;


  /**
   * @var int FIRST_IMAGE_DATE The first date that an astronomy image is available (June 16, 1995).
   */
  const FIRST_IMAGE_DATE = 803278800;

  function __construct(ConfigFactory $configFactory, Messenger $messenger, LoggerChannelInterface $logger) {
    $this->config = $configFactory->get('nasa_apod.settings');
    $this->messenger = $messenger;
    $this->logger = $logger;
  }

    /**
     * @param DrupalDateTime|NULL $date
     * @param boolean $useHD defaults to FALSE
     * @param int|NULL $count A positive integer, no greater than 100. If this is specified then count randomly chosen images.
     * @param DrupalDateTime|NULL $start_date
     * @param DrupalDateTime|NULL $end_date
     *
     * @return false|mixed
     */
  public function getImage(DrupalDateTime $date = NULL, bool $useHD = FALSE, int $count = NULL, DrupalDateTime $start_date = NULL, DrupalDateTime $end_date = NULL) {
      // This is the first day where an image/video is available.
      $max_date = DrupalDateTime::createFromTimestamp(self::FIRST_IMAGE_DATE);

    if ( is_null($date) ) {
      $date = new DrupalDateTime('now');
    }

    $date->setTime(0, 0, 0);

    if ( $date->getTimestamp() < $max_date->getTimestamp() ) {
        $this->messenger->addMessage('NASA\'s API only contains image from June 16, 1995 forward.', 'warning');
        $date = new DrupalDateTime();
    }

    if (!empty($count)) {
      $cid = 'nasa_apod:random' . (!$useHD ? '' : '-HD');
    }
    else {
      $cid = 'nasa_apod:' . $date->format('Y-m-d') . (!$useHD ? '' : '-HD');
    }

    $data = NULL;
    if ( $cache = \Drupal::cache('data')->get($cid)) {
      $data = $cache->data;
    } else {
      $options = array(
        'query' => array(
          'api_key' => $this->config->get('key'),
          'date' => $date->format('Y-m-d'),
          'hd' => $useHD ? 'True': 'False',
          'thumbs' => 'True',
          'concept_tags' => 'True'
        )
      );

      if ( !is_null($count) && intval($count) > 0 ) {
        // $count can not be used with 'date', 'start_date', or 'end_date' params.
        unset($options['query']['date']);
        if ( intval($count) > 100 ) {
          $this->messenger->addWarning($this->t('COUNT parameter can not be greater than 100. Automatically reducing to the maximum.'));
          $count = 100;
        }
        $options['query']['count'] = intval($count);
      }

      // $count can not be used with 'date', 'start_date', or 'end_date' params.
      if ( !isset($options['query']['count']) && !empty($start_date) ) {
        unset($options['query']['date']);
        $options['query']['start_date'] = $start_date->format('Y-m-d');
        if ( !is_empty($end_date) ) {
          $options['query']['end_date'] = $end_date->format('Y-m-d');
        }
      }

      // Create a HTTP client.
      $client = \Drupal::httpClient();

      try {
        $response = $client->get(($this->config->get('url') ?? 'DEMO_KEY'), $options);
      } catch (ConnectionException $e ) {
        $this->logger->log(RfcLogLevel::ERROR, $e->getMessage());
        $this->alert( RfcLogLevel::ERROR, $this->t('Failed to connect to the NASA API.') );
        return FALSE;
      } catch (\Exception $e ) {
        $this->logger->log(RfcLogLevel::ERROR, $e->getMessage());
        $this->alert( RfcLogLevel::ERROR, $this->t('An unknown error occurred in the NASA API. Please check the logs for more information.') );
        return FALSE;
      }

      if ( $response->getStatusCode() == 200 ) {
        $data = json_decode( $response->getBody() );
        $expire = $date->format('U') + 60*60; // expire the cache in one hour

        \Drupal::cache()->set($cid, $data, $expire);
      } else {
        $message = 'HTTP request resulted in a @status response; @body';
        $params = array(
          '@status' => $response->getStatusCode(),
          '@body' => $response->getReasonPhrase(),
        );
        $this->logger->log(RfcLogLevel::ERROR, $message, $params);
        return FALSE;
      }
    }

    return $data;
  }

  /**
   * @param $string
   * @param array $args
   * @param array $options
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected function t($string, array $args = [], array $options = []) {
    return new TranslatableMarkup($string, $args, $options);
  }

  protected function alert($type, $message) {
    if ( $this->config->get('display_errors') ) {
      switch( $type ) {
        case RfcLogLevel::ERROR:
        case RfcLogLevel::ALERT:
        case RfcLogLevel::CRITICAL:
        case RfcLogLevel::EMERGENCY:
          $this->messenger->addError($message);
          break;
        case RfcLogLevel::WARNING:
          $this->messenger->addWarning($message);
          break;
        default:
          $this->messenger->addStatus($message);
      }
    }
  }
}