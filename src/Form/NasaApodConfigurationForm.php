<?php

namespace Drupal\nasa_apod\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class NasaApodConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nasa_apod_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('nasa_apod.settings');

    $form['nasa_apod_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API endpoint'),
      '#description' => $this->t('Astronomy Picture of the Day API endpoint URL. Currently this URL is <em>https://api.nasa.gov/planetary/apod</em>.'),
      '#default_value' => empty($config->get('url')) ? 'https://api.nasa.gov/planetary/apod' : $config->get('url'),
      '#required' => TRUE,
    ];

    $form['nasa_apod_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('While not required, having a NASA API key allows you to have much higher rate limits. See <a href=":link" target="_blank" title="NASA Open API documentation">the official documentation</a> for more information on rate limits and obtaining your personal API key.', [':link' => 'https://api.nasa.gov']),
      '#default_value' => $config->get('key'),
      '#attributes' => [
        'placeholder' => 'DEMO_KEY'
      ]
    ];

    $form['nasa_apod_hi_res'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('High resolution images'),
      '#description' => $this->t('Retrieve high resolution images when available.'),
      '#default_value' => empty($config->get('hi_res')) ? FALSE : $config->get('hi_res'),
    ];

    $form['display_errors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display errors'),
      '#description' => $this->t('If checked, API errors will set a message visible to all users. This should be disabled in a production environment to prevent sensitive data from being exposed.'),
      '#default_value' => empty($config->get('display_errors')) ? FALSE : $config->get('display_errors'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('nasa_apod.settings');
    $config->set('url', trim($form_state->getValue('nasa_apod_url')));
    $config->set('key', trim($form_state->getValue('nasa_apod_api_key')));
    $config->set('hi_res', $form_state->getValue('nasa_apod_hi_res'));
    $config->set('display_errors', intval($form_state->getValue('display_errors')));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {$inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nasa_apod.settings',
    ];
  }

}
